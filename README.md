# How to setup environment variables

1. Create a `.env` file in the backend directory
2. Fill in the following keys:
   - DB_NAME
