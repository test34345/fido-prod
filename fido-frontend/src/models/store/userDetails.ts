export type UserDetailsValue = {
  isRequestLoading: boolean;
  errorMessage: string | null;
  action: "delete-user" | "add-authenticator" | null;
};

export type UserDetailsContextValue = {
  details: UserDetailsValue;
  setDetails: React.Dispatch<React.SetStateAction<UserDetailsValue>>;
};

export type UserDetailsProviderProps = {
  children: React.ReactNode;
};
