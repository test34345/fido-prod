import { AuthFormMode } from "./general";
import { CreatedUser, User } from "./user";

export type ClientDataJson = {
  challenge: string;
  origin: string;
  type: string;
};

export type Credentials = Credential & {
  response: AuthenticatorAttestationResponse;
  rawId: ArrayBuffer;
  authenticatorAttachment: AuthenticatorAttachment;
};

export type CredentialsResponse = {
  attestationObject: string;
  clientDataJSON: string;
  transports: any[];
};

export type VerificationCredentials = {
  id: string;
  rawId: string;
  attestationObject: string;
  clientDataJSON: string;
  authenticatorAttachment: AuthenticatorAttachment;
  type: PublicKeyCredentialType;
  response: CredentialsResponse;
};

export type UserVerificationData = {
  _id: string;
  challenge: string;
};

export type RegistrationVerificationReqBody = {
  credentials: VerificationCredentials;
  user: UserVerificationData;
};

export type GenerateOptionsConfig =
  | {
      optionsType: "auth";
      data: { email: string };
    }
  | {
      optionsType: "registration";
      data: User;
    };

export type AttestationResponse = {
  generatedOptions: any;
  user: UserVerificationData;
};

export type AuthenticationOptions = {
  allowCredentials: any[];
  challenge: string;
  extensions: any;
  rpId: string;
  timeout: number;
  userVerification: any;
};

export type GenerateAuthenticationOptionsUserData = {
  name: string;
  _id: string;
};

export type GenerateAuthenticationOptionsResponse = {
  authOptions: AuthenticationOptions;
  user: GenerateAuthenticationOptionsUserData;
};

export type AuthLoginVerificationResponse = {
  user: CreatedUser;
  verified: boolean;
};

export type VerificationReqBody = {
  user: GenerateAuthenticationOptionsUserData;
  credentials: any;
};

export type StartAuthenticationConfig = GenerateAuthenticationOptionsResponse;

export type AuthFormData = {
  name: string;
  email: string;
  password: string;
  authMode: AuthFormMode;
};
