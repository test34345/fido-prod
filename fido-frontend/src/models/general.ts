import { AuthenticationCancelledError } from "../utils/Errors/authCancelled";
import { LoginError } from "../utils/Errors/loginError";
import { UserDoesNotExistError } from "../utils/Errors/userDoesNotExist";
import { UserAlreadyExistsError } from "../utils/Errors/userExists";

export type ErrorType =
  | Error
  | AuthenticationCancelledError
  | UserAlreadyExistsError
  | LoginError
  | UserDoesNotExistError;
export type AuthFormMode = "login" | "register";

export type AuthModeChangedHandler = (newMode: AuthFormMode) => void;
