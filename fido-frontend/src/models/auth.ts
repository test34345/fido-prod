import { AuthFormMode } from "./general";
import { LoginUserResponse } from "./user";
import { AuthFormData } from "./webauthn";

export type GetUserAuthActionHandler = (
  form: AuthFormData,
  authMode: AuthFormMode
) => Promise<LoginUserResponse>;
