export type User = {
  userName: string;
  userEmail: string;
};

export type UserWithChallenge = User & {
  challenge: string;
};

export type CreatedUser = {
  userName: string;
  email: string;
  password: string;
  challenge: string;
  loginChallenge: string;
  authenticators: any[];
  _id: string;
};

export type UserInContext = CreatedUser & {
  isLoggedIn: boolean;
};

export type CreatedUserResponse = {
  user: CreatedUser;
};

export type LoginUserResponse = {
  user: CreatedUser;
};
