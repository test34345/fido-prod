import { useState, createContext } from "react";
import { initialUserData, userContextInitialValue } from "./initialValues";
import {
  UserContextProviderProps,
  UserContextValue,
} from "../../models/store/userContext";
import { UserInContext } from "../../models/user";

export const UserContext = createContext<UserContextValue>(
  userContextInitialValue
);

export const UserContextProvider: React.FC<UserContextProviderProps> = ({
  children,
}) => {
  const [user, setUser] = useState<UserInContext>(initialUserData);
  const value: UserContextValue = { user, setUser };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};
