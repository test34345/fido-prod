import { useState, createContext } from "react";
import {
  userDetailsContextInitialValue,
  userDetailsInitialValue,
} from "./initialValue";
import {
  UserDetailsContextValue,
  UserDetailsProviderProps,
  UserDetailsValue,
} from "../../models/store/userDetails";

export const UserDetailsContext = createContext<UserDetailsContextValue>(
  userDetailsContextInitialValue
);

export const UserDetailsProvider: React.FC<UserDetailsProviderProps> = ({
  children,
}) => {
  const [details, setDetails] = useState<UserDetailsValue>(
    userDetailsInitialValue
  );

  const value: UserDetailsContextValue = { details, setDetails };

  return (
    <UserDetailsContext.Provider value={value}>
      {children}
    </UserDetailsContext.Provider>
  );
};
