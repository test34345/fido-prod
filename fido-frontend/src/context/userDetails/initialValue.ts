/* eslint-disable @typescript-eslint/no-empty-function */
import {
  UserDetailsContextValue,
  UserDetailsValue,
} from "../../models/store/userDetails";

export const userDetailsInitialValue: UserDetailsValue = {
  errorMessage: "",
  isRequestLoading: false,
  action: null,
};

export const userDetailsContextInitialValue: UserDetailsContextValue = {
  details: userDetailsInitialValue,
  setDetails: () => {},
};
