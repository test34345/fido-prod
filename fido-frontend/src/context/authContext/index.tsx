import { createContext, useState } from "react";
import {
  AuthContextProviderProps,
  AuthContextValue,
  AuthContextValueExposed,
} from "../../models/store/authContext";
import { initialContextValue, initialExposedValue } from "./initialValues";

export const AuthContext = createContext(initialExposedValue);

export const AuthContextProvider: React.FC<AuthContextProviderProps> = ({
  children,
}) => {
  const [authContextValue, setAuthContextValue] =
    useState<AuthContextValue>(initialContextValue);

  const logoutHandler = () => {
    setAuthContextValue(initialContextValue);
  };

  const exposedValue: AuthContextValueExposed = {
    value: authContextValue,
    setValue: setAuthContextValue,
    logout: logoutHandler,
  };

  return (
    <AuthContext.Provider value={exposedValue}>{children}</AuthContext.Provider>
  );
};
