import { useState, useContext } from "react";
import AuthForm from "../../components/auth/AuthForm/AuthForm";
import useResetErrorMessage from "../../hooks/useResetErrorMessage";
import { AuthFormMode, AuthModeChangedHandler } from "../../models/general";
import { capitalize } from "../../utils/general";
import { INITIAL_AUTH_MODE } from "../../utils/constants";
import "./AuthPage.scss";
import { UserContext } from "../../context/userContext";
import { Navigate } from "react-router-dom";

const AuthPage = () => {
  const [authMode, setAuthMode] = useState<AuthFormMode>(INITIAL_AUTH_MODE);
  const userContext = useContext(UserContext);
  useResetErrorMessage();

  const authModeChangedHandler: AuthModeChangedHandler = (newMode) => {
    setAuthMode(newMode);
  };

  if (userContext.user?.isLoggedIn) {
    return <Navigate to="/profile" />;
  }

  return (
    <section className="auth-section">
      <h3>AuthPage - {capitalize(authMode)}</h3>
      <AuthForm onAuthModeChange={authModeChangedHandler} />
    </section>
  );
};

export default AuthPage;
