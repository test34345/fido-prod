import { Routes, Route } from "react-router-dom";
import Header from "./layout/Header/Header";
import AuthPage from "./pages/AuthPage/AuthPage";
import AuthSuccessPage from "./pages/AuthSuccessPage/AuthSuccessPage";
import AuthFailurePage from "./pages/AuthFailurePage/AuthFailurePage";
import UserDetailsPage from "./pages/UserDetailsPage/UserDetailsPage";
import { UserDetailsProvider } from "./context/userDetails/userDetails";

const App = () => {
  return (
    <>
      <Header />

      <main>
        <div className="container">
          <Routes>
            <Route path="/" Component={AuthPage} />
            <Route path="/auth-success" Component={AuthSuccessPage} />
            <Route path="/auth-failure" Component={AuthFailurePage} />
            <Route
              path="/profile"
              Component={() => (
                <UserDetailsProvider>
                  <UserDetailsPage />
                </UserDetailsProvider>
              )}
            />
          </Routes>
        </div>
      </main>
    </>
  );
};

export default App;
