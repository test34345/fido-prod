import { useContext, useEffect } from "react";
import { AuthContext } from "../context/authContext";

const useResetErrorMessage = () => {
  const { setValue: setAuthContextValue } = useContext(AuthContext);

  useEffect(() => {
    setAuthContextValue((prevValue) => ({
      ...prevValue,
    }));
  }, []);
};

export default useResetErrorMessage;
