import { useContext } from "react";
import { UserDetailsContext } from "../context/userDetails/userDetails";

const useUserDetails = () => {
  const userDetailsCtx = useContext(UserDetailsContext);

  const toggleLoader = (newLoaderLoadingValue: boolean) => {
    userDetailsCtx.setDetails((prevValue) => ({
      ...prevValue,
      isRequestLoading: newLoaderLoadingValue,
    }));
  };

  const resetLoaderAndUserDetailsData = () => {
    userDetailsCtx.setDetails((prevValue) => ({
      ...prevValue,
      action: null,
      isRequestLoading: false,
    }));
  };

  const isAddingAuthenticator =
    userDetailsCtx.details.isRequestLoading &&
    userDetailsCtx.details.action === "add-authenticator";

  const isDeletingUser =
    userDetailsCtx.details.isRequestLoading &&
    userDetailsCtx.details.action === "delete-user";

  return {
    value: userDetailsCtx.details,
    toggleLoader,
    setUserDetails: userDetailsCtx.setDetails,
    resetLoaderAndUserDetailsData,
    isAddingAuthenticator,
    isDeletingUser,
  };
};

export default useUserDetails;
