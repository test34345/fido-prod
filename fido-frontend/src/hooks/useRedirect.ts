import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../context/authContext";

const useRedirect = () => {
  const { value: autContextValue } = useContext(AuthContext);
  const navigateTo = useNavigate();

  useEffect(() => {
    if (!autContextValue.isLoggedIn && !autContextValue.isLoggedInChanged) {
      navigateTo("/");
    }
  }, []);
};

export default useRedirect;
