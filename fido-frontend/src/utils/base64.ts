/**
 * Encode the bytes as text using base64url. Used to encode huge buffers
 * @param buffer any ArrayBuffer
 * @returns
 */
export const base64url = (buffer: ArrayBuffer): string => {
  return btoa(
    ([] as Uint8Array[]).reduce.call<Uint8Array, any[], string>(
      new Uint8Array(buffer),
      function (p: any, c: any) {
        return p + String.fromCharCode(c);
      },
      ""
    )
  )
    .replaceAll("+", "-")
    .replaceAll("/", "_")
    .replaceAll("=", "");
};
