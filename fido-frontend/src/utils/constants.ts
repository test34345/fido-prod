import { AuthFormMode } from "../models/general";

export const INITIAL_AUTH_MODE: AuthFormMode = "login";

export const API = {
  url: {
    dev: "http://localhost:4000",
    prod: "https://a401-46-10-214-246.ngrok-free.app",
  },
};
