import axios from "axios";
import {
  AuthLoginVerificationResponse,
  GenerateOptionsConfig,
  RegistrationVerificationReqBody,
  VerificationReqBody,
} from "../models/webauthn";
import { UserAlreadyExistsError } from "./Errors/userExists";
import { API } from "./constants";

export class AuthDataFetch {
  static async verifyRegistration(
    verificationData: RegistrationVerificationReqBody
  ) {
    const resp = await axios.post(
      `${API.url.dev}/api/auth/registration-verification`,
      verificationData
    );

    return resp;
  }

  static async generateOptions<T>(config: GenerateOptionsConfig) {
    const endpoint =
      config.optionsType === "auth" ? "auth-options" : "registration-options";

    try {
      const response = await axios.post(
        `${API.url.dev}/api/auth/${endpoint}`,
        config.data
      );
      return response.data as T;
    } catch (err) {
      if (endpoint === "registration-options")
        throw new UserAlreadyExistsError();
    }
  }

  static async verifyLoginAuthentication(
    verificationBody: VerificationReqBody
  ) {
    const response = await axios.post<AuthLoginVerificationResponse>(
      `${API.url.dev}/api/auth/login-verification`,
      verificationBody
    );

    return response.data;
  }

  static deleteUser(userId: string) {
    return axios.delete(`${API.url.dev}/api/user/${userId}`);
  }
}
