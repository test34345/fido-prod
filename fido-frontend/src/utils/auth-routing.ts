export const getAuthRouteFromUserVerified = (verified: boolean) =>
  verified ? "/auth-success" : "/auth-failure";
