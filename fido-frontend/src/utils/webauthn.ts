import { WebAuthnLogin } from "./webauthn-login";
import { WebAuthnRegister } from "./webauthn-register";

export const WebAuthn = {
  login: WebAuthnLogin,
  register: WebAuthnRegister,
};
