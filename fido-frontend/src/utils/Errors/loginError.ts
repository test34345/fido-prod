export class LoginError extends Error {
  constructor() {
    super();
    this.message =
      "This email does not exist or there is a problem with your authenticator";
  }
}
