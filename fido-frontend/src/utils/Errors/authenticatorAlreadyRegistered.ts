export class AuthenticatorAlreadyRegistered extends Error {
  constructor() {
    super();
    this.message =
      "You have already registered this authenticator. Please try again with another one.";
  }
}
