export class UserDoesNotExistError extends Error {
  constructor() {
    super();
    this.message = "The user with this email and/or password does not exist";
  }
}
