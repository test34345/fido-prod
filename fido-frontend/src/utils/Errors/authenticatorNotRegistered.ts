export class AuthenticatorNotRegisteredError extends Error {
  constructor() {
    super();
    this.message = "Authenticator not registered.";
  }
}
