import { useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { UserInContext } from "../../../models/user";
import { WebAuthn } from "../../../utils/webauthn";
import { ErrorType } from "../../../models/general";
import LoadingSpinner from "../../general/LoadingSpinner/LoadingSpinner";
import useUserDetails from "../../../hooks/useUserDetails";
import { UserContext } from "../../../context/userContext";

type UserAuthenticatorsProps = {
  onAddAuthenticator?: () => void;
  onAddAuthenticatorError?: () => void;
  onAddAuthenticatorFinally?: () => void;
};

const UserAuthenticators: React.FC<UserAuthenticatorsProps> = (props) => {
  const {
    onAddAuthenticator,
    onAddAuthenticatorError,
    onAddAuthenticatorFinally,
  } = props;

  const { user, setUser } = useContext(UserContext);

  const {
    value: userDetailsValue,
    resetLoaderAndUserDetailsData,
    setUserDetails,
  } = useUserDetails();
  const navigateTo = useNavigate();

  if (!user) {
    return <Navigate to="/" />;
  }

  const handleAuthErrors = (err: ErrorType) => {
    onAddAuthenticatorError && onAddAuthenticatorError();
    onAddAuthenticatorFinally && onAddAuthenticatorFinally();

    alert(err.message);
    navigateTo("/auth-failure");
  };

  const registerAuthenticator = async () => {
    setUserDetails((prevValue) => ({
      ...prevValue,
      action: "add-authenticator",
      isRequestLoading: true,
    }));

    try {
      onAddAuthenticator && onAddAuthenticator();

      const verificationData = await WebAuthn.register.registerUser({
        userEmail: user.email,
        userName: user.userName,
      });

      const newUserData: UserInContext = {
        ...user,
        ...verificationData.user,
      };

      setUser(newUserData);
    } catch (err) {
      handleAuthErrors(err as ErrorType);
    } finally {
      resetLoaderAndUserDetailsData();
      onAddAuthenticatorFinally && onAddAuthenticatorFinally();
    }
  };

  return (
    <>
      <div className="authenticators">
        {user.authenticators.map((curAuthenticator) => (
          <p key={curAuthenticator.credentialID}>
            {curAuthenticator.credentialID}
          </p>
        ))}
      </div>

      {userDetailsValue.isRequestLoading &&
        userDetailsValue.action === "add-authenticator" && <LoadingSpinner />}

      <button
        type="button"
        onClick={registerAuthenticator}
        disabled={userDetailsValue.isRequestLoading}
      >
        Add Authenticator
      </button>
    </>
  );
};

export default UserAuthenticators;
