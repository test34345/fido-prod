import React, { useState, useContext } from "react";
import { getOppositeAuthMode } from "../../../utils/authForm";
import { AuthContext } from "../../../context/authContext";
import {
  AuthFormMode,
  AuthModeChangedHandler,
  ErrorType,
} from "../../../models/general";
import { useNavigate } from "react-router-dom";
import { WebAuthn } from "../../../utils/webauthn";
import LoadingSpinner from "../../general/LoadingSpinner/LoadingSpinner";
import { AuthFormData } from "../../../models/webauthn";
import { UserContext } from "../../../context/userContext";
import { getUserAuthAction } from "../../../utils/auth";

type AuthFormProps = {
  onAuthModeChange: AuthModeChangedHandler;
};

const initialFormData: AuthFormData = {
  name: "Lee",
  email: "lee@webauthn.guide",
  password: "DSS123",
  authMode: "login",
};

const AuthForm: React.FC<AuthFormProps> = ({ onAuthModeChange }) => {
  const [form, setForm] = useState<AuthFormData>(initialFormData);
  const [isRequestLoading, setIsRequestLoading] = useState(false);
  const userContext = useContext(UserContext);
  const authContext = useContext(AuthContext);
  const navigateTo = useNavigate();
  const isUserLoggedIn = authContext.value.isLoggedIn;

  const userDataChangeHandler = (type: "email" | "name" | "password") => {
    return (event: React.FormEvent) => {
      const input = event.target as HTMLInputElement;
      const newValue = input.value;

      setForm((prevValue) => ({
        ...prevValue,
        name: type === "name" ? newValue : prevValue.name,
        email: type === "email" ? newValue : prevValue.email,
        password: type === "password" ? newValue : prevValue.password,
      }));
    };
  };

  const authHandler = (authType: AuthFormMode) => {
    return async () => {
      const authAction = getUserAuthAction(form, authType);

      setIsRequestLoading(true);

      try {
        const { user: newUser } = await authAction;

        console.log("newUser", newUser);

        userContext.setUser({
          ...newUser,
          isLoggedIn: true,
        });

        navigateTo("/profile");
      } catch (err) {
        if (err instanceof Error) {
          alert(err.message);
        }
      } finally {
        setIsRequestLoading(false);
      }
    };
  };

  const webauthnLogin = async () => {
    setIsRequestLoading(true);

    try {
      const verificationResponse = await WebAuthn.login.loginUser(form.email);

      if (!verificationResponse.verified || !verificationResponse.user) return;

      userContext.setUser({
        ...verificationResponse.user,
        isLoggedIn: true,
      });

      navigateTo("/profile");
    } catch (err) {
      alert((err as ErrorType).message);
    } finally {
      setIsRequestLoading(false);
    }
  };

  const switchAuthModeHandler = () => {
    onAuthModeChange(getOppositeAuthMode(form.authMode));

    setForm((prevValue) => ({
      ...prevValue,
      authMode: getOppositeAuthMode(prevValue.authMode),
    }));
  };

  return (
    <div>
      {!isUserLoggedIn && (
        <a
          href="#"
          className="auth-mode"
          onClick={switchAuthModeHandler}
          style={{ color: "inherit" }}
        >
          Switch to {getOppositeAuthMode(form.authMode)} mode
        </a>
      )}

      {isUserLoggedIn && (
        <div>
          <h3>You've already logged in</h3>
          <button type="button" onClick={authContext.logout}>
            Logout
          </button>
        </div>
      )}

      {!isUserLoggedIn && (
        <form onSubmit={authHandler(form.authMode)}>
          <div className="input-group">
            {form.authMode === "register" && (
              <input
                type="text"
                placeholder="Your name"
                value={form.name}
                disabled
                onChange={userDataChangeHandler("name")}
              />
            )}

            <input
              type="email"
              placeholder="Your email"
              value={form.email}
              disabled
              onChange={userDataChangeHandler("email")}
            />

            <input
              type="password"
              value={form.password}
              disabled
              placeholder="Password"
              onChange={userDataChangeHandler("password")}
            />
          </div>

          {isRequestLoading && <LoadingSpinner />}

          {!isRequestLoading && (
            <div className="buttons">
              {form.authMode === "login" && (
                <>
                  <button type="button" onClick={authHandler("login")}>
                    Login with password
                  </button>

                  <button type="button" onClick={webauthnLogin}>
                    Login with WebAuthn
                  </button>
                </>
              )}

              {form.authMode === "register" && (
                <button type="button" onClick={authHandler("register")}>
                  Sign up
                </button>
              )}
            </div>
          )}
        </form>
      )}
    </div>
  );
};

export default AuthForm;
