import { Body, Controller, Post } from '@nestjs/common';
import {
  GenerateAuthenticationOptionsReqBody,
  GenerateRegistrationOptionsReqBody,
  VerificationReqBody,
} from 'src/models/webauthn';
import { AuthService } from './auth.service';
import { GenerateRegistrationOptionsDto } from './dtos/generate-registration-options.dto';
import { GenerateAuthOptionsDto } from './dtos/generate-auth-options.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('registration-options')
  generateRegistrationOptions(@Body() reqBody: GenerateRegistrationOptionsDto) {
    return this.authService.generateRegistrationOptions({ reqBody });
  }

  @Post('registration-verification')
  registrationVerification(@Body() reqBody: VerificationReqBody) {
    return this.authService.registrationVerification({ reqBody });
  }

  @Post('auth-options')
  generateAuthenticationOptions(@Body() reqBody: GenerateAuthOptionsDto) {
    return this.authService.generateAuthenticationOptions(reqBody);
  }

  @Post('login-verification')
  verifyAuthenticationResponse(@Body() reqBody: VerificationReqBody) {
    return this.authService.verifyAuthenticationResponse(reqBody);
  }
}
