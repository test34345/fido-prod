import { User } from 'src/user/user.entity';
import { Entity, Column, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Authenticator {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  credentialID: string;

  @Column()
  credentialPublicKey: string;

  @Column()
  counter: number;

  @ManyToOne(() => User, user => user.authenticators)
  user: User;
}
