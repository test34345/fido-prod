export type Authenticator = {
  credentialID: string;
  credentialPublicKey: string;
  counter: number;
};

export type AuthenticatorDecoded = {
  credentialID: Uint8Array;
  credentialPublicKey: Uint8Array;
  counter: number;
};

export type LoginUserReqBody = {
  email: string;
  password: string;
};

export type CreateUserReqBody = {
  name: string;
  email: string;
  password: string;
};

export type CreateUserConfig = {
  reqBody: CreateUserReqBody;
};

export type LoginUserConfig = {
  reqBody: LoginUserReqBody;
};
