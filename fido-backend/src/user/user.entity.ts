import { Authenticator } from 'src/authenticators/authenticator.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  challenge: string;

  @Column()
  loginChallenge: string;

  @OneToMany(() => Authenticator, authenticator => authenticator.user)
  authenticators: Authenticator[];
}
