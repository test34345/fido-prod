export const challenge = Uint8Array.from('randomStringFromServer', c =>
  c.charCodeAt(0)
);

export const CREDENTIAL_OPTIONS = {
  rpName: 'Duo Security',
  rpID: 'localhost',
  attestationType: 'none',
  origin: `http://localhost:5173`,
};

export const CREDENTIAL_OPTIONS_PROD = {
  rpName: 'Duo Security',
  rpID: 'fido-frontend.netlify.app',
  attestationType: 'direct',
  origin: `https://fido-frontend.netlify.app`,
};
